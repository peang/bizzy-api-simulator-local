const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const parsedConfig = require('./lambda/parser/routes.json');
const lambda = require('./lambda');
const Promise = require('bluebird');

const handler = Promise.promisify(lambda.handler);
const app = express();

app.use(cors());
app.use(bodyParser.json());

app.get('/api/get-sidebar', (req, res) => {
    const items = [];

    for (let key in parsedConfig.data) {
        const folder = parsedConfig.data[key].folder;
        const endpoints = parsedConfig.data[key].endpoints;

        const data = {
            name: folder,
            url: '#',
            children: []
        };

        for (let endpointKey in endpoints) {
            const endpointPath = endpoints[endpointKey].path;
            const endpointFunction = endpoints[endpointKey].function;
            const endpointMocks = endpoints[endpointKey].mocks;

            data.children.push({
                name: endpointFunction,
                url: `/linter?folder=${folder}&path=${endpointPath}&function=${endpointFunction}&mocks=${endpointMocks}`
            });
        }

        items.push(data);
    }

    res.send({items, contextMock: parsedConfig.mockContext});
});

// Registering method paths from lambda functions
for (let key in parsedConfig.data) {
    const endpoints = parsedConfig.data[key].endpoints;

    for (let endpointKey in endpoints) {
        const endpointFunction = endpoints[endpointKey].function;
        const endpointPath = endpoints[endpointKey].path;

        const endpointRegisteredPath = `/${endpointPath}`;
        lambda.Handler.registerMethods(endpointRegisteredPath);

        const endpointCompletePath = `/${endpointPath}/${endpointFunction}`;
        app.post(endpointCompletePath, (req, res, next) => {
            let dataPayload;
            let contextPayload;
            let actionName;
            let fileName;

            if (req.body) {
                dataPayload = req.body.data;
                contextPayload = req.body.context;
                actionName = req.body.actionName;
                fileName = req.body.fileName;
            } else {
                res.status(422).send({
                    message: 'No request body data',
                    data: {}
                });
            }

            try {
                handler({
                    action: actionName,
                    file: fileName,
                    data: dataPayload,
                    context: contextPayload
                }, {
                        invokedFunctionArn: 'local'
                    })
                    .then(result => {
                        res.status(200).send(result);
                    })
                    .catch(error => {
                        if (error.defaultCode) {
                            res.status(error.defaultCode).send({
                                "code": error.defaultCode,
                                "message": error._message
                            });
                        } else {
                            res.status(500).send({
                                "code": 500,
                                "message": error.message,
                                "error_detail": error.stack
                            });
                        }
                    });
            } catch (error) {
                res.status(error.defaultCode).send({
                    "code": 500,
                    "message": error._message
                });
            }
        });
    }
}

app.listen(5000, () => console.log(`Listening on port 5000`));
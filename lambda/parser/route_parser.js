const path = require('path');
const fs = require('fs');

const methodsPath = path.join(path.resolve(), '/src/methods'); 

let data = {};
let methods = fs.readdirSync(
    methodsPath
)

let endpoints = [];

for (const methodFolder of methods) {
    let newPath = path.join(methodsPath, methodFolder);
    let isDir = fs.lstatSync(newPath).isDirectory();

    if (isDir) {
        let methodFile = fs.readdirSync(newPath);

        endpoints[methodFolder] = [];

        for (const methodFileName of methodFile) {
            let trimmedMethod = methodFileName.substring(
                methodFileName.length - 3, methodFileName
            );

            let combinedPath = `${methodFolder}/${trimmedMethod}`;

            endpoints[methodFolder][combinedPath] = [];

            const factoryPath = path.join(newPath, `${methodFileName}`);
            if (path.extname(factoryPath) === '.js') {
                const factory = require(factoryPath);
                for (const key in factory) {
                    endpoints[methodFolder][combinedPath].push(key);
                }
            }
        }
    }
}

let config = {
    message: 'Read from files',
    data: [],
    mockContext: null
};

for (let folder in endpoints) {
    let populatedData = {
        folder: folder,
        endpoints: []
    };

    for (let file in endpoints[folder]) {
        for (let key in endpoints[folder][file]) {
            let funcName = endpoints[folder][file][key];
            let mocks = null;
            let contextMocks = null;

            try {
                mocks = require(`${path.resolve()}/tests/mocks/${funcName}.json`)
            } catch (err) {
                // will let mock data as null
            }

            populatedData.endpoints.push({
                path: file,
                function: funcName,
                mocks: JSON.stringify(mocks)
            });
        }
    }

    config.data.push(populatedData);
}

let mockContext;
try {
    mockContext = require(`${path.resolve()}/tests/mocks/context.json`);
} catch (err) {

}

config.mockContext = mockContext;

fs.writeFileSync(
    path.join(__dirname, "routes.json"),
    JSON.stringify(config),
    "utf8"
);

console.log(`√ Success creating files`);

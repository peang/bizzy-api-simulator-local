'use strict';

const fs = require('fs');
const path = require('path');
const Promise = require('bluebird');
const DotEnv = require('dotenv');

function loadEnvironment(context) {
    const invokedArn = context.invokedFunctionArn.toUpperCase();
    const inferredEnv = 'local';

    const envPath = `${path.resolve(__dirname, '..', '..', '..', '..')}/environments/local.env`;
    const loadedEnv = DotEnv.parse(fs.readFileSync(envPath));

    for (const key in loadedEnv) {
        process.env[key] = loadedEnv[key];
    }
    process.env.NODE_ENV = inferredEnv;
}

function parseUser(context) {
    if (!context || !context.encodedUser) {
        return null;
    }

    try {
        return JSON.parse(Buffer.from(context.encodedUser, 'base64').toString());
    } catch (error) {
        console.log(context.encodedUser);
        console.log(`Parse userContext failed \n${error.message}`);
    }

    return null;
}

exports.getHandler = function (methods, dbContext, mongoContext, redisContext) {
    const lambdaHandler = function (event, context, callback) {
        let result,
            error;

        loadEnvironment(context);

        return Promise.coroutine(function* () {
            if (dbContext) {
                yield dbContext.getContext();
            }

            if (mongoContext) {
                yield mongoContext.getContext();
            }

            if (redisContext) {
                yield redisContext.getContext();
            }

            return Promise.resolve('Context Opened');
        })().then(() => {
            const main = {};
            const factoryPath = path.join(__dirname, './../../../../src/methods/', `${event.file}.js`);
            const factory = require(factoryPath);
            for (const key in factory) {
                main[key] = () => factory[key];
            }

            const parsedUser = parseUser(event.context);
            if (parsedUser) {
                event.context.user = parsedUser;
            }

            try {
                if (typeof event.data === 'string') {
                    event.data = JSON.parse(event.data);
                }
            } catch (err) {
                // means not a json structure
            }

            try {
                if (typeof event.context === 'string') {
                    event.context = JSON.parse(event.context);
                }
            } catch (err) {
                // means not a json structure
            }

            return main[event.action]()(event.data, event.context || {}).then((res) => {
                result = res;
            }).catch((err) => {
                error = err;
            });
        }).catch((err) => {
            error = new Error(JSON.stringify({
                code: 500,
                detail: err.stack
            }));
        }).finally(Promise.coroutine(function* () {
            if (dbContext) {
                yield dbContext.closeContext();
            }

            if (mongoContext) {
                yield mongoContext.closeContext();
            }

            if (redisContext) {
                yield redisContext.closeContext();
            }

            return callback(error, result);
        }));
    };

    return lambdaHandler;
};

exports.getErrorTransformer = () => ({
    getter() {
        return JSON.stringify({
            code: this.name,
            detail: this._message
        });
    },
    setter(message) {
        this._message = message;
    }
});

module.exports = exports;

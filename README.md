How to implement

- Add to package.json devDependencies: 
    "bizzy-api-simulator": "git+ssh://git@gitlab.com/peang/bizzy-api-simulator-local.git",

- Add to package.json scripts:
    "simulator": "node node_modules/bizzy-api-simulator/lambda/parser/route_parser && concurrently --kill-others-on-fail \"nodemon node_modules/bizzy-api-simulator/server\" \"cd node_modules/bizzy-api-simulator && npm run client\"",

- Add this params to your env:
    SIMULATOR_DB_FOLDER_NAME: {name of your database models folder. e.g: purchaseorder-models}
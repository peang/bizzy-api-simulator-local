import axios from 'axios';

export function getSidebarData(props) {
    axios.get(`http://localhost:5000/api/get-sidebar`)
        .then((res) => {
            props.dispatch({
                type: 'linter_sidebar.has_data',
                data: res.data.items,
                contextMock: res.data.contextMock
            });
        })
        .catch((err) => {
            console.log(err);
        });
}

export function submitRequest(path, payload, props) {
    props.dispatch({
        type: 'linter_request.request'
    })
    
    var startTime = new Date().getTime();
    axios.post(`http://localhost:5000${path}`, payload)
        .then((res) => {
            let requestTime = new Date().getTime() - startTime;

            if (res.data.code) {
                if (res.data.code !== 200 || res.data.code !== 201) {
                    props.dispatch({
                        type: 'linter_request.request_error',
                        body: res.data.message,
                        code: res.data.code,
                        badge: 'danger',
                        request_time: requestTime
                    })
                } else {
                    props.dispatch({
                        type: 'linter_request.request_success',
                        body: res.data,
                        code: res.data.code,
                        badge: 'success',
                        request_time: requestTime
                    });
                }
            }
        })
        .catch((err) => {
            let errorMessage;

            if (err.body && err.body.message) {
                errorMessage = err.body.message;
            } else {
                errorMessage = err.message;
            }

            let requestTime = new Date().getTime() - startTime;

            props.dispatch({
                type: 'linter_request.request_error',
                body: errorMessage,
                code: '-',
                badge: 'danger',
                request_time: requestTime
            })
        })
}
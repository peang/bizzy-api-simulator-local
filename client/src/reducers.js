import { combineReducers } from 'redux';

import { linterPayloadMocks, linterContextMocks, linterPayloadData, linterContextData, linterSidebar, linterRequest } from './views/Linter/reducer';

const allReducers = {
    linterPayloadMocks,
    linterPayloadData,
    linterContextMocks,
    linterContextData,
    linterSidebar,
    linterRequest
}

export default combineReducers(allReducers);
export function linterPayloadMocks(state = {
    mocks: '// Data Payload'
}, action) {
    let new_state = JSON.parse(JSON.stringify(state));

    switch (action.type) {
        case 'linter_payload.use_mocks':
            new_state = {
                mocks: action.mocks
            }
            return new_state;

        default:
            return state;
    }
}

export function linterPayloadData(state = {
    data: ''
}, action) {
    let new_state = JSON.parse(JSON.stringify(state));

    switch (action.type) {
        case 'linter_payload_data.change':
            new_state = {
                data: action.data
            }
            return new_state;

        default:
            return state;
    }
}

export function linterContextMocks(state = {
    mocks: '// Context Payload'
}, action) {
    let new_state = JSON.parse(JSON.stringify(state));

    switch (action.type) {
        case 'linter_context.use_mocks':
            new_state = {
                mocks: action.mocks
            }
            return new_state;

        default:
            return state;
    }
}

export function linterContextData(state = {
    data: ''
}, action) {
    let new_state = JSON.parse(JSON.stringify(state));

    switch (action.type) {
        case 'linter_context_data.change':
            new_state = {
                data: action.data
            }
            return new_state;

        default:
            return state;
    }
}

export function linterRequest(state = {
    header: {},
    body: {},
    badge: 'success',
    code: '-',
    request_time: 0
}, action) {
    let new_state = JSON.parse(JSON.stringify(state));

    switch (action.type) {
        case 'linter_request.request':
            new_state = {
                header: {},
                body: 'Loading...',
                badge: 'success',
                code: '-',
                request_time: 0
            }
            return new_state;

        case 'linter_request.request_success':
            try {
                if (typeof action.body === 'string') {
                    action.body = JSON.parse(action.body);
                }
            } catch (err) {
                // means ordinary string
            }

            new_state = {
                header: action.header,
                body: action.body,
                badge: action.badge,
                code: action.code,
                request_time: action.request_time
            }
            return new_state;

        case 'linter_request.request_error':
            try {
                if (typeof action.body === 'string') {
                    action.body = JSON.parse(action.body);
                }
            } catch (err) {
                // means ordinary string
            }

            new_state = {
                header: action.header,
                body: action.body,
                badge: action.badge,
                code: action.code,
                request_time: action.request_time
            }
            return new_state;
        default:
            return state;
    }
}

export function linterSidebar(state = {
    data: [],
    contextMock: ''
}, action) {
    let new_state = JSON.parse(JSON.stringify(state));

    switch (action.type) {
        case 'linter_sidebar.has_data':
            new_state = {
                data: action.data,
                contextMock: action.contextMock
            }
            return new_state;

        default:
            return state;
    }
}
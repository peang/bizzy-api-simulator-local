import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { Col, Row } from 'react-bootstrap';
import url from 'url';
import { UnControlled as CodeMirror } from 'react-codemirror2';
import LinterResult from './LinterResult';
import { submitRequest } from './../../request_handler'

class Linter extends Component {
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
        this.useDataMock = this.useDataMock.bind(this);
        this.updateDataMock = this.updateDataMock.bind(this);
        this.useContextMock = this.useContextMock.bind(this);
        this.updateContextMock = this.updateContextMock.bind(this);
    }

    handleSubmit() {
        let data = this.props.linterPayloadData.data;
        let context = this.props.linterContextData.data;

        let fileName = this.query.path;
        let path = `/${this.query.path}/${this.query.function}`;

        try {
            while (typeof data === 'string') {
                data = JSON.parse(data);
            }
        } catch (err) {
            this.props.dispatch({
                type: 'linter_request.request_error',
                body: 'Failed when try to parse payload data to JSON',
                code: '-',
                badge: 'danger',
                request_time: 0
            });
            return;
        }

        try {
            while (typeof context === 'string') {
                context = JSON.parse(context);
            }
        } catch (err) {
            this.props.dispatch({
                type: 'linter_request.request_error',
                body: 'Failed when try to parse payload data to JSON',
                code: '-',
                badge: 'danger',
                request_time: 0
            });
            return;
        }

        let payload = {
            data,
            context,
            fileName,
            actionName: this.query.function
        }

        submitRequest(path, payload, this.props);
    }

    componentWillMount() {
        this.query = {
            function: 'Null'
        };
    }

    componentWillMount() {
        this.query = url.parse(this.props.location.search, true).query;
    }

    componentWillUpdate(newProps) {
        this.query = url.parse(newProps.location.search, true).query;
    }

    updateDataMock(editor, data, value) {
        this.props.dispatch({
            type: 'linter_payload_data.change',
            data: JSON.stringify(JSON.parse(value), null, 2)
        });
    }

    updateContextMock(editor, data, value) {
        this.props.dispatch({
            type: 'linter_context_data.change',
            data: JSON.stringify(JSON.parse(value), null, 2)
        });
    }

    useDataMock() {
        this.props.dispatch({
            type: 'linter_payload.use_mocks',
            mocks: JSON.stringify(JSON.parse(this.query.mocks), null, 2)
        });

        this.props.dispatch({
            type: 'linter_payload_data.change',
            data: JSON.stringify(JSON.parse(this.query.mocks), null, 2)
        });
    }

    useContextMock() {
        if (this.props.linterSidebar.contextMock === undefined) {
            this.props.dispatch({
                type: 'linter_context.use_mocks',
                mocks: JSON.stringify({})
            });

            this.props.dispatch({
                type: 'linter_context_data.change',
                data: JSON.stringify({})
            });
        } else {
            this.props.dispatch({
                type: 'linter_context.use_mocks',
                mocks: JSON.stringify(this.props.linterSidebar.contextMock, null, 2)
            });

            this.props.dispatch({
                type: 'linter_context_data.change',
                data: JSON.stringify(this.props.linterSidebar.contextMock, null, 2)
            });
        }
    }

    /**
     * @returns {XML}
     */
    render() {
        return (
            <div className="animated fadeIn">
                <Col xs={12}>
                    <div className="card">
                        <div className="card-header">Payloads : <strong>{this.query.function}</strong></div>
                        <Row className="card-block">
                            <Col sm={6} md={6}>
                                <strong><p>Data Payload</p></strong>
                                <div style={{ margin: 10 + 'px' }}>
                                    <CodeMirror
                                        onChange={this.updateDataMock}
                                        options={{
                                            lineNumbers: true
                                        }}
                                        theme='monokai'
                                        value={this.props.linterPayloadMocks.mocks}
                                    />
                                </div>
                                <button className='btn btn-md btn-success float-right' onClick={this.useDataMock}>Use Mocks</button>
                            </Col>
                            <Col sm={6} md={6}>
                                <strong><p>Context Payload</p></strong>
                                <CodeMirror
                                    onChange={this.updateContextMock}
                                    options={{
                                        lineNumbers: true
                                    }}
                                    theme='monokai'
                                    value={this.props.linterContextMocks.mocks}
                                />

                                <button className='btn btn-md btn-success float-right' style={{ marginTop: 15 + 'px' }} onClick={this.useContextMock}>Use Mocks</button>
                            </Col>
                        </Row>
                        <div className="card-footer">
                            <button className='btn btn-md btn-info float-right' onClick={this.handleSubmit}>
                                <i className='fa fa-dot-circle-o'></i> Submit
                            </button>
                        </div>
                    </div>
                </Col>

                <Col xs={12}>
                    <LinterResult />
                </Col>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        linterPayloadMocks: state.linterPayloadMocks,
        linterPayloadData: state.linterPayloadData,
        linterContextMocks: state.linterContextMocks,
        linterContextData: state.linterContextData,
        linterSidebar: state.linterSidebar
    };
}

export default connect(mapStateToProps)(Linter)

import React, { Component } from "react";
import { connect } from "react-redux";
import JSONTree from "react-json-tree";
import PropTypes from "prop-types";
import { Controlled as CodeMirror } from 'react-codemirror2';

class LinterResult extends Component {
    /**
     * @param props
     */
    constructor(props) {
        super(props);
    }

    render() {
        let view;
        let body = this.props.linterRequest.body;

        if (typeof body === 'object') {
            body = JSON.stringify(body, null, 2);
        }

        return (
            <div className={`card card-accent-${this.props.linterRequest.badge} card-result-wrapper`}>
                <div className="card-header">
                    Request Result
                        <span className='badge badge-info float-right'>{this.props.linterRequest.request_time}ms</span>
                    <span className={`badge badge-${this.props.linterRequest.badge} float-right`} style={{ marginRight: 5 + 'px' }}>{this.props.linterRequest.code}</span>
                </div>
                <div style={{margin: 10 + 'px'}}>
                    <CodeMirror
                        theme='monokai'
                        value={body}
                        options={{
                            mode: 'javascript',
                            lineWrapping:true
                        }}
                    />
                </div>
            </div>
        );
    }
}

LinterResult = connect((state) => {
    return {
        linterRequest: state.linterRequest
    }
})(LinterResult)

export default LinterResult;